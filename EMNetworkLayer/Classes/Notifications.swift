import Foundation

extension Notification.Name {
    /// Used as a namespace for all `APIProtocol` related notifications.
    public struct API {
        /// Posted when a `APIProtocol` instance will start executing a request. The notification `object` contains `URLRequest` object.
        public static let WillStart = Notification.Name(rawValue: "org.emnetworklayer.notification.name.api.willstart")
        
        /// Posted when a `APIProtocol` instance did receive an error. The notification `object` contains `APIResponse` with specified error.
        public static let OnError = Notification.Name(rawValue: "org.emnetworklayer.notification.name.api.onerror")
        
        /// Posted when a `APIProtocol` request is completed. The notification `object` contains the completed `APIResponse` with all data.
        public static let DidComplete = Notification.Name(rawValue: "org.emnetworklayer.notification.name.api.didcomplete")
    }
}
