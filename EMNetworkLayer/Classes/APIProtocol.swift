import Foundation
import Alamofire

public typealias HTTPMethod = Alamofire.HTTPMethod

public protocol APIProtocol {
    var headers: [String: String]? { get }
    var method: HTTPMethod { get }
    /// Base URL of the server.
    /// - NOTE: The best way is making extension with default value.
    var baseURL: String { get }
    /// URL path which is being added to `baseURL`
    var path: String { get }
    /// A list of request parameters. Can be used for POST as well as for GET.
    var params: [String: Any]? { get }
    /// Optional. A Path for object serializer in case you need to dig into JSON response contents.
    var parserPath: String? { get }
    /// Optional.
    var version: UInt8 { get }
    /// Being used in case of Uploading acitons e.g. sending images to server.
    var multipartForm: MultipartForm? { get }
    
    /// - NOTE: The best way to set listener is by extending `APIProtocol` with default implementtion of this value.
    var listener: APIListener? { get }
}

extension APIProtocol {
    
    public var headers: [String: String]? { nil }
    
    public var params: [String: Any]? { nil }
    
    /// Default path which can be used to extract target objects. Should be formed with a rules of Dicionary value path.
    public var parserPath: String? { nil }
    
    public var version: UInt8 { 1 }
    
    public var multipartForm: MultipartForm? { nil }
    
    public var url: URL? {
        let r = path.startIndex ..< path.index(path.startIndex, offsetBy: 2)
        let p = path.replacingOccurrences(of: "/", with: "", options: [], range: r)
        return URL(string: baseURL)?.appendingPathComponent(p)
    }
    
    var listener: APIListener? { return nil }
}


// MARK: - MultipartForm

public protocol MultipartForm {
    var data: Data { get }
    var name: String { get }
    var fileName: String? { get }
    var mimeType: String? { get }
}

public struct MultipartReadyParams: MultipartForm {
    public var data: Data
    public var name: String = "file"
    public var fileName: String? = nil
    public var mimeType: String?
    public init(data: Data, mimeType: String? = "") {
        self.data = data
        if let mt = mimeType, mt.isEmpty {
            self.mimeType = data.mimeType
        } else {
            self.mimeType = mimeType
        }
    }
}

public extension MultipartForm {
    
    var defaultFileName: String {
        var name: String {
            let length = Int(arc4random_uniform(20)+1)
            let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            return String((0 ..< length).compactMap { _ in letters.randomElement() })
        }
        if let mt = mimeType, let ext = String.fileExtensionFrom(mimeTypeString: mt) {
            return "\(name).\(ext)"
        }
        return name
    }
    
    func fill(multipartFormData: MultipartFormData) {
        if let mt = mimeType {
            let targetFileName = fileName ?? defaultFileName
            multipartFormData.append(data, withName: name, fileName: targetFileName, mimeType: mt)
        } else {
            multipartFormData.append(data, withName: name)
        }
    }
    
    var description: String {
        var arr = ["data length = \(data.count)", "name = \(name)"]
        if let fn = fileName { arr += ["file name = \(fn)"] }
        if let mt = mimeType { arr += ["mime type = \(mt)"] }
        return arr.joined(separator: ",\n")
    }
}


// MARK: - Logging support

public protocol APIListener {
    func api(_ api: APIProtocol, willStart request: URLRequest?, curl: String?)
    func api(_ api: APIProtocol, didFinishWith response: HTTPURLResponse?, data: Data?, error: Error?)
    func api(willStartUpload api: APIProtocol)
    func api(_ api: APIProtocol, error: Error)
}
