import Foundation

public enum JSONDecodingError: Error {
    case invalidJSON
    case serializationFailed
    case invalidPath
    case objectNotFound
}

private let decoder = JSONDecoder()
private let encoder = JSONEncoder()


extension Decodable {
    public static func decode(data: Data) throws -> Self {
        return try decoder.decode(Self.self, from: data)
    }
}


extension Encodable {
    public func encode() throws -> Data {
        encoder.outputFormatting = .prettyPrinted
        return try encoder.encode(self)
    }
}


public extension Data  {

    typealias Item = Decodable

    /// Converts response info into specified object type on the go
    func item<T: Item>(itemType: T.Type) throws -> T {
        return try decoder.decode(T.self, from: self)
    }
}


extension JSONDecoder {
    
    public func decode<T: Decodable>(_ type: T.Type, from json: [String: Any]) throws -> T  {
        if !JSONSerialization.isValidJSONObject(json) { throw JSONDecodingError.invalidJSON }
        let data = try JSONSerialization.data(withJSONObject: json, options: [])
        return try T.decode(data: data)
    }
}


public extension Dictionary where Key == String, Value: Any {
    
    typealias Item = Decodable
    
    /// Converts response info into specified object type on the go, using `parserpath` from API object.
    func item<T: Item>(itemType: T.Type, subpath: String? = nil) throws -> T {
        if let p = subpath {
            guard let subDict = self[keyPath: DictKeyPath(p)] as? [String: Any] else { throw JSONDecodingError.invalidPath }
            return try subDict.item(itemType: T.self)
        }
        return try decoder.decode(T.self, from: self)
    }
    
    /// Converts response info into objects array of specified type on the go, using `parserpath` from API object.
    func items<T: Item>(itemType: T.Type, subpath: String? = nil) throws -> [T] {
        var list: [[String: Any]]?
        if let p = subpath {
            guard let sp = self[keyPath: DictKeyPath(p)] as? [String: Any] else {
                throw JSONDecodingError.invalidPath
            }
            list = sp.rootList as? [[String: Any]]
        } else {
            list = self.rootList as? [[String: Any]]
        }
        guard let arr = list else { throw JSONDecodingError.objectNotFound }
        return try arr.compactMap { (dict) -> T? in
            return try decoder.decode(T.self, from: dict)
        }
    }
}


public extension APIResponse {
    
    typealias Item = Decodable
    
    /// Converts response info into specified object type on the go, using `parserpath` from API object.
    func item<T: Item>(itemType: T.Type) throws -> T {
        if let e = error { throw e }
        guard let data = self.info else { throw JSONDecodingError.objectNotFound }
        if let subPath = api.parserPath {
            guard let json = self.serialized().info, let subDict = json[keyPath: DictKeyPath(subPath)] as? [String: Any] else {
                throw JSONDecodingError.invalidPath
            }
            return try subDict.item(itemType: T.self)
        }
        return try data.item(itemType: itemType)
    }
    
    /// Converts response info into objects array of specified type on the go, using `parserpath` from API object.
    func items<T: Item>(itemType: T.Type) throws -> [T] {
        if let e = error { throw e }
        guard let json = self.serialized().info else { throw JSONDecodingError.serializationFailed }
        if let subPath = api.parserPath {
            guard let subDict = json[keyPath: DictKeyPath(subPath)] as? [String: Any] else {
                throw JSONDecodingError.invalidPath
            }
            return try subDict.items(itemType: T.self)
        }
        return try json.items(itemType: itemType)
    }
}


public extension Dictionary where Key: ExpressibleByStringLiteral, Value: Any {
    var rootList: [Any]? {
        if let key = self.keys.first, let list = self[key] as? [Any]  { return list }
        for (_, value) in self {
            if value is [Any] { return value as? [Any] }
        }
        return nil
    }
}
