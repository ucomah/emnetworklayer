# EMNetworkLayer

[![CI Status](https://img.shields.io/travis/jeka-mel/EMNetworkLayer.svg?style=flat)](https://travis-ci.org/jeka-mel/EMNetworkLayer)
[![Version](https://img.shields.io/cocoapods/v/EMNetworkLayer.svg?style=flat)](https://cocoapods.org/pods/EMNetworkLayer)
[![License](https://img.shields.io/cocoapods/l/EMNetworkLayer.svg?style=flat)](https://cocoapods.org/pods/EMNetworkLayer)
[![Platform](https://img.shields.io/cocoapods/p/EMNetworkLayer.svg?style=flat)](https://cocoapods.org/pods/EMNetworkLayer)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

EMNetworkLayer is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'EMNetworkLayer'
```

## Author

jeka-mel, iosdeveloper.mail@gmail.com

## License

EMNetworkLayer is available under the MIT license. See the LICENSE file for more info.
